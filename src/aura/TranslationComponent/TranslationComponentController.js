({
	init : function(component, event, helper) {
        component.set('v.dataTableColumns', [
            { label: 'From Language', fieldName: 'From_Language__c', type: 'text', sortable: false },
            { label: 'Original Text', fieldName: 'Original_Text__c', type: 'text', sortable: false },
            { label: 'To Language', fieldName: 'To_Language__c', type: 'text', sortable: false },
            { label: 'Translated Text', fieldName: 'Translated_Text__c', type: 'text', sortable: false },
            { label: 'Status', fieldName: 'Status__c', type: 'text', sortable: false }
        ]);
        //console.log(component.get("v.dataTableColumns"));

        //get translation history
        helper.getTranslationHistoryRecords(component, 1);
        helper.getAvailableLanguagesValues(component);

    },

	translate : function(component, event, helper) {
        var originalList = component.get("v.originalTranslationHistory");
	    component.set('v.translationHistory', originalList);
        var fromLanguage = component.get("v.fromLanguage");
        var toLanguage = component.get("v.toLanguage");
        var message = component.get("v.inputText");

        //custom validationa
	    var isValid = true;
	    isValid = helper.validityCheck(component, message)

	    if(isValid) {
            var params = {
                fromLanguage,
                toLanguage,
                message
            }
            component.set("v.showSpinner", true);
            helper.createTranslationRecord(component, params)
            .then(
                $A.getCallback(function(result) {
                    return helper.enQueueTranslation(component, result.Id)
                })
            ).then(
                $A.getCallback(function(result) {
                    setTimeout(function(){
                        helper.getTranslatedUpdates(component);
                    }, 3000)
                })
            ).catch ( err => {
                $A.reportError("Oops", error);
                console.log("Oops", error);
            })
	    }

	},

	handleComboBoxFromChange: function(component, event, helper) {
        var selectedOptionValue = event.getParam("value");
        component.set("v.fromLanguage", selectedOptionValue);
        console.log(selectedOptionValue);
    },

    handleComboBoxToChange: function(component, event, helper) {
        var selectedOptionValue = event.getParam("value");
        component.set("v.toLanguage", selectedOptionValue);
        console.log(selectedOptionValue);
    },

    firstPage: function(component, event, helper) {
        helper.getTranslationHistoryRecords(component, 1);
        component.set("v.currentPage", 1)
    },

    prevPage: function(component, event, helper) {
        var currentPage = Number(component.get("v.currentPage"));

        currentPage -= 1;
        component.set("v.currentPage", currentPage)
        helper.getTranslationHistoryRecords(component, currentPage);
    },
    nextPage: function(component, event, helper) {
        var currentPage = Number(component.get("v.currentPage"));
        currentPage += 1;
        component.set("v.currentPage", currentPage)

        helper.getTranslationHistoryRecords(component, currentPage);
    }

})