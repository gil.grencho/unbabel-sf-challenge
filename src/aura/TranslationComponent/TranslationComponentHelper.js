({
	getTranslationHistoryRecords: function(component, pageNum) {
		var action = component.get("c.getTranslationsHistory");
		component.set("v.showSpinner", true);
		if(pageNum) {
		    action.setParams({pageNum});
		}
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.translationHistory', response.getReturnValue());
                if(pageNum == 1) {
                    component.set('v.originalTranslationHistory', response.getReturnValue());
                }
            } else {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
            component.set("v.showSpinner", false);
        });
        $A.enqueueAction(action);
	},

	getAvailableLanguagesValues: function(component) {
        var action = component.get("c.getLanguageValues");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var picklistVals = JSON.parse(response.getReturnValue());
                var parsedPicklistVals = picklistVals.map(item => {
                    return {label: item.value.split(':')[1], value: item.value.split(':')[0]}
                });

                component.set('v.availableLanguages', parsedPicklistVals);
            } else {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
	},

    enQueueTranslation: function(component, recordId) {
        var action = component.get("c.requestTranslation");

        action.setParams({recordId});

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                  console.log('enQueued Translation');
            } else {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
	},

    createTranslationRecord: function(component, params) {
        return new Promise($A.getCallback(function(resolve, reject) {
            var action = component.get("c.createTranslation");

            action.setParams({fromLang: params.fromLanguage, toLang: params.toLanguage, message: params.message});

            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {

                    var resp = response.getReturnValue();
                    var translationHistory = component.get("v.translationHistory");
                    translationHistory.unshift(resp);
                    component.set("v.translationHistory", translationHistory);
                    console.log(resp);
                    resolve(resp);

                } else {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                    reject("Rejected");
                }
            });
            $A.enqueueAction(action);
        }));
    },



	getTranslatedUpdates: function(component) {
        var self = this;
        return new Promise($A.getCallback(function(resolve, reject) {
//        debugger

            var action = component.get("c.getTranslatedUpdates");
            var translationHistory = component.get("v.translationHistory");

            action.setParams({recordId: translationHistory[0].Id});

            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var resp = response.getReturnValue();
                    console.log('updates' ,resp);

                    if(resp != null) {
                        translationHistory[0] = resp;
                        component.set("v.translationHistory", translationHistory);
                        component.set('v.originalTranslationHistory', translationHistory);
                        component.set("v.currentPage", 1)
                        component.set("v.outputText", translationHistory[0].Translated_Text__c);
                        component.set("v.showSpinner", false)
                    } else {
                        setTimeout(function(){
                            self.getTranslatedUpdates(component);
                        }, 3000);
                    }
                    resolve('Resolved')

                } else {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                    reject("Rejected");
                }
            });
            $A.enqueueAction(action);
        }));
	},

    //check field input validations before save on server.
    validityCheck: function(component, message) {
        var feedBack = 'Invalid Inputs';
        var isValid = true
        //SF standard field validations
        var allValid = component.find('inputField').reduce(function (validSoFar, inputCmp) {
                            inputCmp.reportValidity();
                            return validSoFar && inputCmp.checkValidity();
                        }, true)

        if (!allValid) {
            isValid = false;
        }

        //Custom validations
        if(message.length == 0) {
            isValid = false;
            feedBack += '\nPlease fill in some text'
        }

        if(isValid) {
            return true;
        } else {
            alert(feedBack);
            return false;
        }
    }

})