public with sharing class TranslationComponentController_Sv {

	@AuraEnabled
	public static List<Translation__c> getTranslationsHistory(Integer pageNum) {
		Integer offset;

		if(pageNum != null) {
			offset = pageNum * 10;
		} else {
			offset = 0;
		}

		System.debug('offset: ' + offset);

		List<Translation__c> translationsList = [
				SELECT Id,
					Name,
					From_Language__c,
					Original_Text__c,
					Status__c,
					To_Language__c,
					Translated_Text__c
				FROM Translation__c
				ORDER BY CreatedDate DESC
				LIMIT 10 OFFSET :offset];

		return translationsList;
	}

//	@AuraEnabled
//	public static Translation__c translateMessage(String fromLang, String toLang, String message) {
//
//		Translation__c translation = new Translation__c(
//			From_Language__c = fromLang,
//				To_Language__c = toLang,
//				Original_Text__c = message,
//				Status__c = 'Translation Requested'
//		);
//
//		insert translation;
//
////		requestTranslation(translation.Id);
//
//		return translation;
//	}

	@AuraEnabled
	public static Translation__c createTranslation(String fromLang, String toLang, String message) {
		Translation__c translation = new Translation__c(
				From_Language__c = fromLang,
				To_Language__c = toLang,
				Original_Text__c = message,
				Status__c = 'Translation Requested'
		);

		insert translation;

		return translation;
	}


	@AuraEnabled
	public static void requestTranslation(Id recordId) {

		Translation__c translation = [SELECT Id, Original_Text__c, From_Language__c, To_Language__c, Status__c FROM Translation__c WHERE Id = :recordId LIMIT 1];

		unbabelapi__Unbabel_Translation_Request__c tr = new unbabelapi__Unbabel_Translation_Request__c();

		System.debug('translation.From_Language__c: ' + translation.From_Language__c);
		System.debug('translation.To_Language__c: ' + translation.To_Language__c);

		tr.unbabelapi__Unbabel_From_Language__c = translation.From_Language__c;
		tr.unbabelapi__Unbabel_To_Language__c = translation.To_Language__c;
		tr.unbabelapi__Unbabel_Translation_Type__c = 'Machine';
		tr.unbabelapi__Unbabel_Namespace__c = '';
		tr.unbabelapi__Unbabel_Class__c = 'GenericTranslationHandler';


		//Optional - if this is not defined, all the queried text fields will be translated
		Set<String> translatedFields = new Set<String>{'Original_Text__c'};

		//Request Translation to Unbabel
		unbabelapi.UnbabelRestEnvelope ure = unbabelapi.UnbabelRestConnectorOutbound.requestTranslation( translation, tr,translatedFields);

		//Sets the TR fields
		tr.unbabelapi__Unbabel_sObject_Id__c = translation.id;
		tr.unbabelapi__Unbabel_sObject__c = 'Translation__c';
		tr.unbabelapi__Unbabel_sObject_Parent_Id__c = translation.id;

		//if request is successfull updates the Translation Request status to "Translation Requested" and inserts the TRs
		if (ure.isSuccess) {
			tr.unbabelapi__Unbabel_Status__c = 'Translation Requested';
			tr.unbabelapi__Unbabel_Translation_Requested_Datetime__c = System.now();

			//If the request fails (ex: Unbabel core is down) updates the Translation Request status to "Request Error" and inserts the TRs
		} else {
			tr.unbabelapi__Unbabel_Status__c = 'Request Error';
			tr.unbabelapi__Unbabel_Error_Log__c = ure.message;
		}

		insert tr;

		translation.Translation_Request__c = tr.Id;
		update translation;

//		return translation;
	}

	@AuraEnabled
	public static Translation__c getTranslatedUpdates(Id recordId) {
		Translation__c translation = [SELECT Id,
											Name,
											Original_Text__c,
											From_Language__c,
											To_Language__c,
											Status__c,
											Translation_Request__c,
											Status_formula__c
									FROM Translation__c
									WHERE Id = :recordId LIMIT 1];

		System.debug(translation);

		if(translation.Status_formula__c == 'Message Translated') {

			Attachment attachment = [
					SELECT Id,
							Name,
							ParentId,
							Body
					FROM Attachment
					WHERE ParentId = :translation.Translation_Request__c
					AND Name = 'Unbabel Raw Message.json' LIMIT 1
			];

			String attachmentStr = attachment.Body.toString();

			System.debug('oi: ');
			translation.Status__c = translation.Status_formula__c;

			Map<String,Object> attachmentBody = (Map<String,Object>) JSON.deserializeUntyped(attachment.body.toString());
			Map<String,Object> data = (Map<String,Object>) attachmentBody.get('data');

			translation.Translated_Text__c = data.get('Original_Text__c').toString();

			update translation;
			return 	translation;

		} else if(translation.Status_formula__c == 'Request Error' || translation.Status_formula__c == 'Translation not Requested') {
			translation.Status__c = translation.Status_formula__c;
			translation.Translated_Text__c = 'An Error Occurred';
			update translation;
			return 	translation;
		} else {
			return null;
		}
	}


	//TODO: add this to Utils Class
	@AuraEnabled
	public static String getLanguageValues() {
		Schema.DescribeFieldResult fieldResult = unbabelapi__Unbabel_Translation_Request__c.unbabelapi__Unbabel_Languages__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		List<PickListWrapper> pkList = new List<PickListWrapper>();

		for( Schema.PicklistEntry pickListVal : ple){
			PickListWrapper pkEntry = new PickListWrapper();
			pkEntry.label = pickListVal.getLabel();
			pkEntry.value = pickListVal.getValue();
			pkList.add(pkEntry);
		}

		return JSON.serialize(pkList);
	}

	public class PickListWrapper {
		String label;
		String value;
	}

}