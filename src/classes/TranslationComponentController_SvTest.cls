@isTest
public class TranslationComponentController_SvTest {

	@TestSetup
	static void setupData() {
		Translation__c tr = new Translation__c(
				From_Language__c = 'en',
				Original_Text__c = 'Hello',
				Status__c = 'Translation Requested',
				To_Language__c = 'pt',
				Translated_Text__c = ''
		);

		insert tr;

	}

	@isTest
	static void getTranslationsHistory() {

		List<Translation__c> trTotalList = [SELECT Id FROM Translation__c];

		Test.startTest();
		List<Translation__c> trList = TranslationComponentController_Sv.getTranslationsHistory(null);
		Test.stopTest();

		System.assertEquals(trTotalList.size(), trList.size());

	}

	@isTest
	static void createTranslation() {

		Test.startTest();
		Translation__c trList = TranslationComponentController_Sv.createTranslation('en', 'pt', 'hello');
		Test.stopTest();

		List<Translation__c> trListCheck = [SELECT Id FROM Translation__c WHERE Id =:trList.Id LIMIT 1];
		System.assertEquals(trListCheck[0].Id, trList.Id);

	}

	@isTest
	static void requestTranslation() {
		List<Translation__c> trTotalList = [SELECT Id FROM Translation__c LIMIT 1];

		Test.startTest();
		TranslationComponentController_Sv.requestTranslation(trTotalList[0].Id);
		Test.stopTest();

		List<unbabelapi__Unbabel_Translation_Request__c> trr = [SELECT Id FROM unbabelapi__Unbabel_Translation_Request__c LIMIT 1];

		System.assertEquals(1, trr.size());

	}

	@isTest
	static void getTranslatedUpdatesSuccess() {
		List<Translation__c> trTotalList = [SELECT Id, Translation_Request__c FROM Translation__c LIMIT 1];
		TranslationComponentController_Sv.requestTranslation(trTotalList[0].Id);

		List<unbabelapi__Unbabel_Translation_Request__c> unbabelTR = [select id, unbabelapi__Unbabel_Status__c from unbabelapi__Unbabel_Translation_Request__c];
		unbabelTR[0].unbabelapi__Unbabel_Status__c = 'Message Translated';

		Blob blobb = Blob.valueOf('{"message":null,"language":null,"isSuccess":true,"dataNoObj":null,"data":{"attributes":{"type":"Translation__c","url":"/services/data/v45.0/sobjects/Translation__c/a062p00001qhP7PAAU"},"Original_Text__c":"Olá, World!","Id":"a062p00001qhP7PAAU"}}');
		Attachment att = new Attachment(Name = 'Unbabel Raw Message.json', Body = blobb, ParentId = unbabelTR[0].Id);
		insert att;

		update unbabelTR;


		Test.startTest();
		TranslationComponentController_Sv.getTranslatedUpdates(trTotalList[0].Id);
		Test.stopTest();

		List<Translation__c> updatedRecord = [SELECT Id,
				Translation_Request__c,
				Status__c
				FROM Translation__c
				WHERE Id = :trTotalList[0].Id
		LIMIT 1];

		System.assertEquals(updatedRecord[0].Status__c, unbabelTR[0].unbabelapi__Unbabel_Status__c);



	}

	@isTest
	static void getTranslatedUpdatesFail() {
		List<Translation__c> trTotalList = [SELECT Id, Translation_Request__c FROM Translation__c LIMIT 1];
		TranslationComponentController_Sv.requestTranslation(trTotalList[0].Id);

		List<unbabelapi__Unbabel_Translation_Request__c> unbabelTR = [select id, unbabelapi__Unbabel_Status__c from unbabelapi__Unbabel_Translation_Request__c];
		unbabelTR[0].unbabelapi__Unbabel_Status__c = 'Request Error';

		Blob blobb = Blob.valueOf('{"message":null,"language":null,"isSuccess":true,"dataNoObj":null,"data":{"attributes":{"type":"Translation__c","url":"/services/data/v45.0/sobjects/Translation__c/a062p00001qhP7PAAU"},"Original_Text__c":"Olá, World!","Id":"a062p00001qhP7PAAU"}}');
		Attachment att = new Attachment(Name = 'Unbabel Raw Message.json', Body = blobb, ParentId = unbabelTR[0].Id);
		insert att;

		update unbabelTR;


		Test.startTest();
		TranslationComponentController_Sv.getTranslatedUpdates(trTotalList[0].Id);
		Test.stopTest();

		List<Translation__c> updatedRecord = [SELECT Id,
				Translation_Request__c,
				Status__c
				FROM Translation__c
				WHERE Id = :trTotalList[0].Id
		LIMIT 1];

		System.assertEquals(updatedRecord[0].Status__c, unbabelTR[0].unbabelapi__Unbabel_Status__c);

	}

	@isTest
	static void getTranslatedUpdatesWaiting() {
		List<Translation__c> trTotalList = [SELECT Id, Translation_Request__c FROM Translation__c LIMIT 1];
		TranslationComponentController_Sv.requestTranslation(trTotalList[0].Id);

		List<unbabelapi__Unbabel_Translation_Request__c> unbabelTR = [select id, unbabelapi__Unbabel_Status__c from unbabelapi__Unbabel_Translation_Request__c];
		unbabelTR[0].unbabelapi__Unbabel_Status__c = 'Translation Requested';

		Blob blobb = Blob.valueOf('{"message":null,"language":null,"isSuccess":true,"dataNoObj":null,"data":{"attributes":{"type":"Translation__c","url":"/services/data/v45.0/sobjects/Translation__c/a062p00001qhP7PAAU"},"Original_Text__c":"Olá, World!","Id":"a062p00001qhP7PAAU"}}');
		Attachment att = new Attachment(Name = 'Unbabel Raw Message.json', Body = blobb, ParentId = unbabelTR[0].Id);
		insert att;

		update unbabelTR;


		Test.startTest();
		TranslationComponentController_Sv.getTranslatedUpdates(trTotalList[0].Id);
		Test.stopTest();

		List<Translation__c> updatedRecord = [SELECT Id,
				Translation_Request__c,
				Status__c
				FROM Translation__c
				WHERE Id = :trTotalList[0].Id
		LIMIT 1];

		System.assertEquals(updatedRecord[0].Status__c, unbabelTR[0].unbabelapi__Unbabel_Status__c);
	}

	@isTest
	static void getLanguageValues() {
		String vals = TranslationComponentController_Sv.getLanguageValues();
		System.debug('vals: ' + vals);
		List<TranslationComponentController_Sv.PickListWrapper> pickListWrapper = (List<TranslationComponentController_Sv.PickListWrapper>)JSON.deserialize(vals, List<TranslationComponentController_Sv.PickListWrapper>.class);
		System.assert(pickListWrapper.size() > 0);
	}
}